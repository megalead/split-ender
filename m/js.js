if (callhunterON != undefined && ctd24ON != undefined && ctd60ON != undefined && ctdtestON != undefined && ctdspanON != undefined && anchor != undefined) {
	var callhunterON;
	var ctd24ON;
	var ctdtestON;
	var ctd60ON;
	var ctdspanON;
	var anchor; 
}


/* ========== СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ДНЯ ========== */

function get_timer24(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctd24 = day+" "+hour+" "+min+" "+sec;
		$(".ctd24").html(ctd24);
	} else {
		$(".ctd24").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputs24(){	
	var d = new Date();
	d.setHours(23,59,59,0);	
	get_timer24(d);
	setInterval(function(){
		get_timer24(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctd24ON == true) {
		getfrominputs24(); 
	}
});

/* ========== /СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ДНЯ ========== */



/* ========== СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ========== */

function get_timer60(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctd60 = day+" "+hour+" "+min+" "+sec;
		$(".ctd60").html(ctd60);
	} else {
		$(".ctd60").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputs60(){	
	var d = new Date();
	d.setMinutes(d.getMinutes()+53);
	d.setSeconds(d.getSeconds()+23);	
	get_timer60(d);
	setInterval(function(){
		get_timer60(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctd60ON == true) {
		getfrominputs60(); 
	}
});

/* ========== /СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ========== */



/* ========== СЧЕТЧИК ЗАМОРОЖЕННЫЙ ========== */

function get_timerTEST(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctdtest = day+" "+hour+" "+min+" "+sec;
		$(".ctdtest").html(ctdtest);
	} else {
		$(".ctdtest").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputsTEST(){	
	var d = new Date();
	d.setMinutes(d.getMinutes()+12345);
	get_timerTEST(d);
	setInterval(function(){
		get_timerTEST(d);
	},10000000000);
}
$(document).ready(function(){ 
	if (ctdtestON == true) {
		getfrominputsTEST(); 
	}
});

/* ========== /СЧЕТЧИК ЗАМОРОЖЕННЫЙ ========== */


	
/* СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ПО СПАНАМ */
function get_timerSPAN(string){
	var date_new = string; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "0" + day;
		} day = day.toString();
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "0" + hour;
		} hour = hour.toString();
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "0" + min;
		} min = min.toString();
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "0" + sec;
		} sec = sec.toString();
		timethis = day + " : " + hour + " : " + min + " : " + sec;
		$(".ctd-day").text(day);
		$(".ctd-hour").text(hour);
		$(".ctd-minute").text(min);
		$(".ctd-second").text(sec);}
	else {
		$(".ctd-day").text("00");
		$(".ctd-hour").text("00");
		$(".ctd-minute").text("00");
		$(".ctd-second").text("00");} 
	}
function getfrominputsSPAN(){
	var d = new Date();
	d.setMinutes(d.getMinutes()+53);
	d.setSeconds(d.getSeconds()+23);	
	get_timerSPAN(d);
	setInterval(function(){
		get_timerSPAN(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctdspanON == true) {
		getfrominputsSPAN(); 
	}
});
/* СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ПО СПАНАМ */



/* ========== Цены ===================================== */

$.getScript('country_price.js'/*tpa=http://cdn.sellpost.pro/js/script/country_price.js*/);

/* ========== /Цены ==================================== */



/* ========== Валидация форм ===================================== */

$(document).ready(function() {
	var countryName = $('select[name=aCountry]');
	$('select[name=aCountry]').on('change', function(e) {
		$('input[name=clientPhoneInitial]').val('');
		if (countryName.val()==175 || countryName.val()==90) {
			$("input[name=clientPhoneInitial]").val("+7 ");
		} else if (countryName.val()==220) {
			$("input[name=clientPhoneInitial]").val("+380 ");
		} else if (countryName.val()==23) {
			$("input[name=clientPhoneInitial]").val("+375 ");
		} else if (countryName.val()==137) {
			$("input[name=clientPhoneInitial]").val("+373 ");		
		}
	});
	$("input[name=clientPhoneInitial]").keypress(function(e) {
    if ( (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 45 || e.keyCode == 43 || e.keyCode == 40 || e.keyCode == 41 || e.keyCode == 32 ) {
      return true;
    } else {
	    return false;
    }
	});
	$("form.validate").submit(function(){ 
		var indexform = $("form.validate").index(this);
		var phoneinput = $("form.validate input[name=clientPhoneInitial]").eq(indexform).val();
		var nameinput = $("form.validate input[name=clientFirstName]").eq(indexform).val();
		if ((phoneinput.length == 0) || (nameinput.length == 0)) {
			alert("\n\n\n – Введите ФИО и номер телефона! \n\n\n");
			return false;
		}
		if ((phoneinput.length < 5) || (phoneinput.length > 20)) {
			alert("\n\n\n – Номер должен быть не короче 4 и не длиннее 20 символов! \n\n\n");
			return false;
		}
	});
});

/* ========== /Валидация форм ===================================== */


/* ========== CallHunter =============================== */

if (callhunterON == true) {
	$(document).ready(function() {
		$('#callhunter').addClass('call-hunter').html('<a href="#modal_callhunter" class="open_modal"><span class="icon-callhunter"></span><p class="text">Заказать<br>звонок</p></a>');
		$('').html('');
	});
}

/* ========== /CallHunter ============================== */




/* ========== Модальное окно ===================================== */

$(document).ready(function() { 
  var overlay = $('#overlay');
  var open_modal = $('.open_modal');
  var close = $('.modal_close, #overlay');
  var modal = $('.modal_div');
  var heightBody = $(window).height() - $(window).height()*0.05 - $(window).height()*0.05 - 70;
	open_modal.click( function(event){	
		event.preventDefault();
		var div = $(this).attr('href');
		var widthDiv = -modal.outerWidth() / 2;
		overlay.fadeIn(400, function() {
			$(div).css('visibility', 'visible').css('margin-left', widthDiv).animate({opacity: 1}, 300);
		});
		$(div).wrapInner('<div class="modal_body"/>').each(function(i,el){
	    if($('.modal_body',el).height() > heightBody){
	      $(div).css({"bottom":"5%", "height":heightBody}); 
	    } else {
	      $(div).css({"bottom":"inherit", "height":"auto"}); 
	    }
		});
		$('body').css('overflow','hidden');
	});
  close.click(function() { 
		modal.animate({opacity: 0}, 300, function() {
    	$(this).css('visibility', 'hidden');
      overlay.fadeOut(400);
    });
    $('body').css('overflow','auto');
  });
});


/* ========== /Модальное окно ===================================== */



/* ========== Плавный якорь ===================================== */

if (anchor == true) {
	$(document).ready(function() { 
		$('a[href^="#"]').on('click', function() {
			var target = $(this).attr('href');
			if (target.search('#modal') == -1) {
				$('html, body').animate({scrollTop: $(target).offset().top}, 800);
				return false;
			}
		});  
	});
}

/* ========== Плавный якорь ===================================== */



/* ========== TimeShift ===================================== */

$(document).ready(function() {
	var offset = new Date().getTimezoneOffset();
	var timeShift = -(offset / 60);
	$('input[name="timeShift"]').attr("value", timeShift);
	console.log(timeShift); 
});

/* ========== /TimeShift ===================================== */