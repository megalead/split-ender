$(document).ready(function(){
    var send_country = $('body').data('countries');

    $(window).load(function() {
        var countryID = fullPrice = $('span#CountryId').text();
        var currency = send_country[countryID]['currency'];
        $('span#price_new').html(send_country[countryID]['priceGoodRate'].toFixed(0));
        $('span#price_old').html((send_country[countryID]['priceGoodRate'] * 2).toFixed(0));
        $('span#price_full').html(send_country[countryID]['fullPriceRate'].toFixed(0));
        $('span#currency').html(currency);
    });

    $('#country').click(function() {
        $('#country_list').slideToggle();
    });

    $('#country_list li').click(function(e) {
        country = $(e.target).data('country');
        currency = send_country[country]['currency'];
        $('#country_flag').html("<img src=img/" + country + ".png />");
        $('#select_country').val(country);
        $('#country_list').slideUp();
        $('span#price_new').html(send_country[country]['priceGoodRate'].toFixed(0));
        $('span#price_old').html((send_country[country]['priceGoodRate'] * 2).toFixed(0));
        $('span#price_full').html(send_country[country]['fullPriceRate'].toFixed(0));
        $('span#currency').html(currency);
    });

    $('select[name=aCountry]').change(function() {
        country = this.options[this.selectedIndex].value;
        currency = send_country[country]['currency'];
        $('#country_flag').html("<img src=img/" + country + ".png />");
        $('span#price_new').html(send_country[country]['priceGoodRate'].toFixed(0));
        $('span#price_old').html((send_country[country]['priceGoodRate'] * 2).toFixed(0));
        $('span#price_full').html(send_country[country]['fullPriceRate'].toFixed(0));
        $('span#currency').html(currency);
    });
});


