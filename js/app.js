$(document).ready(function(){
  console.log('Document ready');

  var DateHelper = {
    addDays : function(aDate, numberOfDays) {
      aDate.setDate(aDate.getDate() + numberOfDays); // Add numberOfDays
      return aDate;                                  // Return the date
    },
    addMinutes : function(aDate, numberOfMinutes) {
      aDate.setMinutes(aDate.getMinutes() + numberOfMinutes);
      return aDate;
    },
    addSeconds : function(aDate, numberOfSeconds) {
      aDate.setSeconds(aDate.getSeconds() + numberOfSeconds);
      return aDate;
    },
    format : function format(date) {
      var day = ("0" + date.getDate()).slice(-2),
          month = ("0" + (date.getMonth()+1)).slice(-2),
          year = date.getFullYear(),
          hour = ("0" + date.getHours()).slice(-2),
          minute = ("0" + date.getMinutes()).slice(-2),
          seconds = ("0" + date.getSeconds()).slice(-2);

      return month + '/' + day + '/' + year + ' ' + hour + ':' + minute + ':' + seconds;
    }
  }

  var calcDate = DateHelper.addMinutes(new Date(), 42);
      calcDate = DateHelper.addSeconds(calcDate, 27);
  var formatDate = DateHelper.format(calcDate);

  console.log('Date', formatDate);

  $('.countdown-timer')
    .flipcountdown({
      size: 'lg',
      showHour:true,
      showMinute:true,
      showSecond:true,
      beforeDateTime: formatDate,
    });

  $('a[href^="#"]').bind('click.smoothscroll',function (e) {
        e.preventDefault();

        var target = this.hash,
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top-40
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

});
